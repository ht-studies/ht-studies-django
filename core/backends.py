from pychpp import CHPP

from django.contrib.auth.backends import BaseBackend
from django.contrib.auth import get_user_model
from django.conf import settings

from .models import Team


class CHPPAuthBackend(BaseBackend):
    """
    Custom Authentification backend to authenticate users through CHPP
    """

    def authenticate(self, request, **kwargs):

        auth = kwargs.get("auth", None)
        code = kwargs.get("code", None)

        if None in (auth, code):
            raise ValueError("auth and code parameters have to be defined")

        chpp = CHPP(consumer_key=settings.CHPP_CONSUMER_KEY,
                    consumer_secret=settings.CHPP_CONSUMER_SECRET)

        access_token = chpp.get_access_token(
            request_token=auth["request_token"],
            request_token_secret=auth["request_token_secret"],
            code=code,
        )

        chpp = CHPP(consumer_key=settings.CHPP_CONSUMER_KEY,
                    consumer_secret=settings.CHPP_CONSUMER_SECRET,
                    access_token_key=access_token["key"],
                    access_token_secret=access_token["secret"],
                    )

        # Get user information from CHPP
        ht_user = chpp.user()
        user_ht_id = ht_user.ht_id
        username = ht_user.username
        teams = ht_user.teams

        user_model = get_user_model()

        try:
            # get user from DB if it already exists
            user = user_model.objects.get(ht_id=user_ht_id)
            user.token_key = access_token["key"]
            user.token_secret = access_token["secret"]
            user.save()

        except user_model.DoesNotExist:
            # otherwise create a new user

            user = user_model.objects.create_user(
                username=username,
                ht_id=user_ht_id,
                token_key=access_token["key"],
                token_secret=access_token["secret"],
            )

            for t in teams:
                team = Team(ht_id=t.ht_id, name=t.name, user=user)
                team.save()

        return user

    def get_user(self, user_id):
        try:
            return get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            return None
