import re

from django.contrib.auth.decorators import login_required
from pychpp import CHPP

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from django.urls import reverse
from django.http import JsonResponse, FileResponse

from .models import Study, Team


def index(request):

    url = "https://addons.mozilla.org/fr/firefox/addon/ht-studies/"

    user_agent = request.META.get('HTTP_USER_AGENT', '')

    if "edg" in user_agent.lower():
        url = "https://microsoftedge.microsoft.com/addons/" \
              "detail/hlcchlpdbffoinplcepihgalbkammpoj"

    elif "chrome" in user_agent.lower():
        url = "https://chrome.google.com/webstore/detail/" \
              "ht-studies/ipoggboabbhcfhlpdfkkaoflfikjijgm"

    return redirect(url)


def privacy(request):
    return render(request, "core/privacy.html")


def safe_callback(callback_url, callback_hash):
    callback = f"{callback_url}#{callback_hash}"

    callback_is_safe = bool(
        re.match(r"^(https://)?(www[0-9]{1,2}|stage)\.hattrick\.org/"
                 r"\#htStudies",
                 callback))

    return callback if callback_is_safe else reverse("index")


def connect(request):
    callback_url = request.GET.get("callback_url", "")
    callback_hash = request.GET.get("callback_hash", "")

    consumer_key = settings.CHPP_CONSUMER_KEY
    consumer_secret = settings.CHPP_CONSUMER_SECRET

    chpp = CHPP(consumer_key, consumer_secret)
    auth = chpp.get_auth(callback_url=request.build_absolute_uri(
        f"{reverse('chpp_callback')}?"
        f"callback_url={callback_url}&"
        f"callback_hash={callback_hash}",
        ),
    )

    # Store auth in session
    request.session["auth"] = auth

    # Redirect to CHPP login webpage
    return redirect(auth["url"])


def disconnect(request):

    callback_url = request.GET.get("callback_url", "")
    callback_hash = request.GET.get("callback_hash", "")

    logout(request)

    return redirect(safe_callback(callback_url, callback_hash))


def chpp_callback(request):
    callback_url = request.GET.get("callback_url", "")
    callback_hash = request.GET.get("callback_hash", "")

    auth = request.session["auth"]
    code = request.GET.get("oauth_verifier", None)

    if code is not None:

        user = authenticate(request=request,
                            auth=auth,
                            code=code,
                            )

        if user is not None:
            login(request, user)

    # Verify callback corresponds to Hattrick website
    return redirect(safe_callback(callback_url, callback_hash))


def app_data(request, team_id=None):

    # Studies data
    studies = [{"id": s.pk,
                "name": s.name,
                "description": s.description,
                "data": list(s.data.values()),
                "start_date": s.start_date,
                "end_date": s.end_date,
                "active": s.active,
                "data_published": s.data_published,
                "data_url": (
                    request.build_absolute_uri(
                        reverse('download_study', args=(s.codename,)))
                    if s.data_published
                    else ""
                ),
                "subscription": False,
                } for s in Study.objects.all()]

    ongoing_studies = [study for study in studies if study["active"] is True]
    ended_studies = [study for study in studies if study["active"] is False]

    data = {"user": None,
            "team": None,
            "studies": {
                "ongoing": ongoing_studies,
                "ended": ended_studies,
                },
            }

    # return more data if user is connected
    if request.user.is_authenticated:
        user_ht_id = request.user.ht_id
        user_username = request.user.username
        team = None

        if team_id is not None:
            team = Team.objects.filter(ht_id=team_id).first()

            if team in request.user.teams.all():
                data["team"] = {"id": team.ht_id,
                                "name": team.name,
                                }

                for s in data["studies"]["ongoing"]:
                    study = Study.objects.get(pk=s["id"])
                    s["subscription"] = (True
                                         if team in study.teams.all()
                                         else False)

        data["user"] = {"id": user_ht_id,
                        "username": user_username,
                        }

    return JsonResponse(data)


def manage_team_studies(order, request, team_id, study_id):
    team = Team.objects.filter(ht_id=team_id).first()
    study = Study.objects.get(pk=study_id)

    response = {"success": False,
                "message": "",
                "study_id": study_id,
                "subscription": study in team.studies.all(),
                }

    if order not in ("subscribe", "unsubscribe"):
        response["message"] = "order parameter must be equal " \
                              "to 'subscribe' or 'unsubscribe'"

    elif team is None:
        response["message"] = "Team ID is not known"

    elif team.user != request.user:
        response["message"] = "Team does not belong to the connected user"

    elif study is None:
        response["message"] = "No study found"

    elif order == "subscribe" and study in team.studies.all():
        response["message"] = "Team has already subscribed to this study"

    elif order == "unsubscribe" and study not in team.studies.all():
        response["message"] = "Team was not participating to this study"

    else:
        if order == "subscribe":
            team.studies.add(study)
            response["subscription"] = True
        else:
            team.studies.remove(study)
            response["subscription"] = False

        response["success"] = True

    return JsonResponse(response)


@login_required
def subscribe(request, team_id, study_id):
    return manage_team_studies("subscribe", request, team_id, study_id)


@login_required
def unsubscribe(request, team_id, study_id):
    return manage_team_studies("unsubscribe", request, team_id, study_id)


def download_study(request, study_codename):

    # if study is not found (wrong codename)
    # redirect to index
    try:
        study = Study.objects.get(codename=study_codename)
    except Study.DoesNotExist:
        return redirect("index")

    # If data is available, serve corresponding data,
    # else, returns index
    if study.data_published:
        response = FileResponse(
            open(study.zip_path, 'rb'),
            filename=f"ht_studies_{study.codename}_data_"
                     f"{study.last_update.strftime('%Y%m%d')}"
                     f".zip",
            as_attachment=True,
        )
    else:
        response = redirect("index")

    return response
